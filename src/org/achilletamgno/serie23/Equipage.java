package org.achilletamgno.serie23;
import java.util.Arrays;

//import org.achilletamgno.serie33.Marin;

public class Equipage {		

	private Marin[] tableauMarins= new Marin[5];
	int taille1=5;

	// Constructeur Equipage
	public Equipage(){
	}

	/*************** Les Methodes *****************/

	/**la methode  addMarin***/
	/* Oui La m�thode addMarin doit etre modifi�e
	 * il ya les contraintes 
	 * - si le tableau est plein ?
	 * - si l'el�m�nt � ajouter existe d�j�
	 */
	/*public boolean addMarin(Marin marins){

		int compteur=0, compteur1=0 ,j=0;
		for(int i=0;i<tableauMarins.length;i++)
			if(marins.equals(tableauMarins[i]))
				compteur++;
		for(j=0;j<tableauMarins.length;j++)
			if(tableauMarins[j]==null){
				compteur1++;
				break;
			}

		if(compteur!=0 || compteur1==0){
			return false;
		}else{
			tableauMarins[j]=marins;
			return true;		
		}
		} */

	/*** la methode  addMarin Modifi�e  **
	 * il y aura un retoure de false lorsque ce marin existe d�j�
	 * 
	 * */


	/*** la methode  addMarin Modifi�e  ***/
	public boolean addMarin(Marin marin){

		if(isMarinPresent(marin)) {
			return false ;
		}

		if(getNombreMarin()==tableauMarins.length){
			this.etendEquipage(taille1/2);
		}

		for(int i=0; i<this.tableauMarins.length; i++){
			if( this.tableauMarins[i] == null){
				this.tableauMarins[i] = marin;

				return true;
			}
		}

		return false;

	}



	/*** la methode  isMarinPresent  ***/
	public boolean isMarinPresent(Marin marins){
		for(int i=0; i<this.tableauMarins.length; i++){
			if(marins.equals(this.tableauMarins[i]) ){
				return true;
			}	
		}
		return false;
	}

	/**la methode getNombreMarin() **/
	public int getNombreMarin(){
		int nombreMarin = 0;
		for(int i=0; i<tableauMarins.length; i++){
			if(tableauMarins[i]==null){
				continue;
			}
			else 
				nombreMarin = i++;
		}
		return nombreMarin;
	}

	/**la methode toString **/ 
	public String toString() {
		return "Equipage [tableauMarins = " + Arrays.toString(tableauMarins) + "]";
	}

	/*** m�thode pour la bonne retraction d'un marin dans l'equipage **/
	/* et le retrait d'un marin ne serait pas possible si :  
	 * - le tableau est vide
	 */
	public boolean removeMarin (Marin marins){
		if((this.isMarinPresent(marins)) == true){
			for(int i=0; i<=tableauMarins.length; /*i++*/){
				if(marins.equals(this.tableauMarins[i])){
					break;
				}
				this.tableauMarins[i]=null;
				return true;
			}
		}
		return false;
	}

	/** la methode clear()  **/
	public void clear(){
		for(int i=0; i<tableauMarins.length; i++){
			this.tableauMarins[i] = null;
		}
	}

	/** Ajout des Marins equipage pass� en param�tre dans equipage courant **/
	public boolean addAllEquipage(Equipage marins){

		int nombreMarinEquip1,
		nombreMarinEquip2,
		nombreMarinEquip1Null,
		compteur=0; 

		nombreMarinEquip1 = this.getNombreMarin();
		nombreMarinEquip2 = marins.getNombreMarin();
		nombreMarinEquip1Null = this.tableauMarins.length - nombreMarinEquip1;

		for(int i=0; i <= nombreMarinEquip1; i++){
			if(this.isMarinPresent( marins.tableauMarins[i])){
				compteur ++;
			}
			if((compteur != 0) || (nombreMarinEquip1Null != nombreMarinEquip2)){
				return false;
			}else{
				for(int index=0; index <= nombreMarinEquip2; index++){
					this.addMarin(tableauMarins[index]);
				}

			}
		}
		return false;
	}



	/** la methode etendEquipage() pour augmenter � la taille de l'entier
	 *  pass� en param�tre **/
	public void etendEquipage(int longueur){

		Marin[] tempon1Marin;
		tempon1Marin = new  Marin[this.tableauMarins.length+longueur];

		for(int j=0; j<this.tableauMarins.length; j++){
			tempon1Marin[j] = this.tableauMarins[j];
		}
		this.tableauMarins = tempon1Marin;

	}


	/** la methode equals **/
	public boolean equals(Object obj){

		if(! (obj instanceof Equipage)){
			return false;
		}else{
			Equipage equipage=(Equipage) obj;

			if(this.getNombreMarin()!=equipage.getNombreMarin()){
				return false;
			}else{
				int compteur=0;
				for(int i=0;i<this.getNombreMarin();i++){
					if(this.isMarinPresent(equipage.tableauMarins[i])==true){
						compteur++;
					}
				}

				if(compteur==this.getNombreMarin()){
					return true;
				}else{
					return false;
				}
			}
		}
	}	


}




