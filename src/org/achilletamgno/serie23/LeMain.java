package org.achilletamgno.serie23;





public class LeMain {

	/************************* Fonction Principale *********************/
	public static void main(String[] args) {

		/************ Declarations  ********************/

		Marin m1 = new Marin("Nicolas", "Baudin");
		Marin m2 = new Marin("J�r�mie", "Beyou" );
		Marin m3 = new Marin("Alain", "Bombard");
		Marin m4 = new Marin("Franck","Cammas");
		Marin m5 = new Marin("Patrice", "Carpentier");

		Marin m6 = new Marin("Jacques", "Cassard");	
		Marin m7 = new Marin("Alain", "Colas");
		Marin m8 = new Marin( null, null);
		//Marin m9 = new Marin("Alain", "Bombard");
		//Marin m10 = new Marin("Nicolas", "Baudin");



		Equipage equipage1  = new Equipage();

		Equipage equipage2  = new Equipage();
		Equipage equipage3  = new Equipage();
		//Equipage equipage4  = new Equipage();
		/*Equipage equipage5  = new Equipage();
		Equipage equipage6  = new Equipage();*/

		/*--4-- ************* La Methode addMarin **************
		 * on ajoute 6 marins dans equipage1 et verifie 
		 *  bien si cela a ete fait ou pas
		 * */

		/******** Equipage 1 ********/
		if(equipage1.addMarin(m1) == true){
			System.out.println("m1 bien ajouter");
		}else{ 
			System.out.println("impossible d ajouter m1");
		}

		if(equipage1.addMarin(m2)==true){
			System.out.println("m2 bien ajouter");
		}else{ 
			System.out.println("impossible d ajouter m2");
		}

		if(equipage1.addMarin(m3)==true){
			System.out.println("m3 bien ajouter");
		}else{ 
			System.out.println("impossible d ajouter m3");
		}

		if(equipage1.addMarin(m4)==true){
			System.out.println("m4 bien ajouter");
		}else{ 
			System.out.println("impossible d ajouter m4");
		}

		if(equipage1.addMarin(m5)==true){
			System.out.println("m5 bien ajouter");
		}else{ 
			System.out.println("impossible d ajouter m5");
		}

		if(equipage1.addMarin(m6)==true){
			System.out.println("m6 bien ajouter");
		}else{ 
			System.out.println("impossible d ajouter m6"+ "\n");
		}

		/*--12-- ********** Test sur toute les autres m�thodes ********** */


		/******** Equipage 2 ********/

		/** Methode addMarin**/
		if(equipage2.addMarin(m6)==true){
			System.out.println("m6 bien ajouter");
		}else{ 
			System.out.println("impossible d ajouter m6");
		}

		if(equipage2.addMarin(m7)==true){
			System.out.println("m7 bien ajouter");
		}else{ 
			System.out.println("impossible d ajouter m7");
		}

		if(equipage2.addMarin(m8)==true){
			System.out.println("m8 bien ajouter");
		}else{ 
			System.out.println("impossible d ajouter m8"+ "\n");
		}

		/** Methode removeMarin()**/
		if(equipage2.removeMarin(m7)==true){
			System.out.println("m7 bien ete retir� ");
		}else{ 
			System.out.println("impossible de retirer m7");
		}

		/** Methode clear()**/
		/*System.out.println(equipage2 + "\n" );
		equipage2.clear();
		System.out.println(equipage2 + "\n" );


		/******** Equipage 3 ********/
		if(equipage3.addMarin(m6)==true){
			System.out.println("m6 bien ajouter");
		}else{ 
			System.out.println("impossible d ajouter m6");
		}
		if(equipage3.addMarin(m7)==true){
			System.out.println("m7 bien ajouter");
		}else{ 
			System.out.println("impossible d ajouter m7");
		}
		if(equipage3.addMarin(m8)==true){
			System.out.println("m8 bien ajouter");
		}else{ 
			System.out.println("impossible d ajouter m8"+ "\n");
		}

		/** Methode equals() **/
		if(equipage2.equals(equipage3)==true){
			System.out.println("equipe2 == equipe3");
		}else{ 
			System.out.println("equipe2 != equipe3");
		}

		/** Methode addAllMarin **/
		if(equipage2.addAllEquipage(equipage3)==true){
			System.out.println("equipe2 + equipe3" + "\n");
			System.out.println(equipage2);
		}else{ 
			System.out.println("impossible d ajouter equipe3 � equipe2 "+ "\n");
		}

		/** Methode isMarinPresent **/
		//equipage1.addMarin(m1);
		System.out.println("m1 is present : " + equipage1.isMarinPresent(m1));

	}
}

