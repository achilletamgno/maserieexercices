package org.achilletamgno.serie34;

public class Main {

	public static void main(String[] args) {

		Capitaine cap2 = new Capitaine( " Davy " ,"  Jones ", 60000, Grade.grade.AMIRALE);
		Marin m1 = new Marin("Nicolas", "Baudin", 1000);
		Marin m2 = new Marin("J�r�mie", "Beyou", 3000);
		Marin m3 = new Marin("Alain", "Bombard", 8000);
		Marin m4 = new Marin("Franck","Cammas",4000);

		EquipageCommande equipCmde = new EquipageCommande(cap2/*ajout de capitaine*/); //
		
		//equipCmde = new EquipageCommande(m1); 
		/* On constate dans cette exemple comment� au dessus que lorsqu'on 
		 * tente de mettre un marin � la place d'un captaine une erreur se 
		 * produit car un Capitaine est un marin mais pas l'inverse
		 * */
		
		
		equipCmde.addMarin(m1); //
		equipCmde.addMarin(m2);
		equipCmde.addMarin(m3);
		equipCmde.addMarin(m4);
		//equipCmde.addMarin(m1);

		System.out.println(equipCmde);

	}

}
