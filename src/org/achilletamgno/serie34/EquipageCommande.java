package org.achilletamgno.serie34;

public class EquipageCommande extends Equipage {

	/***** les champs  *****/
	private Capitaine commandant;
	
	

	/*****  les constructeurs   *****/
	public EquipageCommande(Capitaine commandant) {
	    super();
		this.commandant = commandant;
		//this.capitaine = cap.ins
	}	

	/***** les m�thodes   *****/

	/***** le getteur  *****/
	public Capitaine getCommandant() {
		return commandant;
	}
	/***** le setteur  *****/
	public void setCommandant(Capitaine commandant) {
		this.commandant = commandant;
	}

	public String toString() {
		return "EquipageCommande [commandant=" + commandant + super.toString() +"]";
	}
}
