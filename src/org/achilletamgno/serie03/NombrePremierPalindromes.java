package org.achilletamgno.serie03;

/* le class NombrePremierPalindrome */
public class NombrePremierPalindromes {

	/* verification nombre si premier */
	public boolean nombrePremier(long nbre){
		long val=2,comp=0;
		while(val<=nbre-1 && comp==0){
			if((nbre%val)==0)
				comp++;
			val++;
		}
		return (comp==0);
	}


	public boolean test1 (long nbre){	

		long index=nbre;
		int comp=0;
		long [] tab=new long[10];
		if(nombrePremier(nbre)==true){

			while(index>=10){
				tab[comp]=index % 10;
				index=(index-tab[comp])/10;
				comp++;
			}

			tab[comp]=index; 
			for(int i=0;i<=comp;i++){
				index=comp-i;
				while(index>0){
					tab[i]=tab[i]*10;
					index--;
				}

			}

			long r=0;
			for(int m=0;m<=comp;m++){
				r=r+tab[m];
			}
			if(nbre==r){
				return true;
			}
			else{ 
				return false;
			}
		}
		return false;

	}



	/*public boolean test2(long nbre){
		   if(nombrePremier(nbre)==true){

			String str;
			str = Long.toString(nbre);
			int val1,val2,compteur=0;
			val1=str.length();

			if(val1%2==0){
				val2=val1/2;
			}
			else{
				val2=(val1+1)/2;
			}
			for(int i=0; i<val2; i++){
				if(str.charAt(i)==str.charAt(val1-1-i))
					compteur++;
			}
			return (compteur==val2);

		}
		else return false;


	}*/
}




