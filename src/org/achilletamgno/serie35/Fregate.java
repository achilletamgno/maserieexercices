package org.achilletamgno.serie35;

public class Fregate extends Bateau {
	private String fregate= "Fregate";

	public Fregate(String nom, int tonnage, EquipageCommande equipage){
		super(nom, tonnage, equipage);
		this.fregate = "Fregate";
	}

	public String getTypeBateau() {
		return fregate;
	}
}
