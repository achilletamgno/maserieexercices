package org.achilletamgno.serie35;


public class Main {

	public static void main(String[] args){


		Capitaine cap2 = new Capitaine( " Davy " ,"  Jones ", 60000, Grade.grade.AMIRALE);
		Marin m1 = new Marin("Nicolas", "Baudin", 1000);
		Marin m2 = new Marin("J�r�mie", "Beyou", 3000);
		Marin m3 = new Marin("Alain", "Bombard", 8000);
		Marin m4 = new Marin("Franck","Cammas",4000);

		EquipageCommande equipCmde = new EquipageCommande(cap2);

		equipCmde.addMarin(m1);
		equipCmde.addMarin(m2);
		equipCmde.addMarin(m3);
		equipCmde.addMarin(m4);

		//Bateau bateau = new Bateau();
		Corvette corvette = new Corvette("leviatan ", 10, equipCmde);

		/* On peut aussi d�clarer les noms de Bateau de cette mani�re */

		// Bateau corvette = new Corvette("leviatan ", 10, equipCmde);
		//Bateau croiseur = new Croiseur("leviatan ", 7, equipCmde);
		//Bateau fegate = new Fregate("leviatan ", 3, equipCmde);

		System.out.println(corvette +"\n");

	}
}
