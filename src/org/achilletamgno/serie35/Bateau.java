package org.achilletamgno.serie35;

public abstract class Bateau {  /* elle ne peut etre 
 * extenci�e mais peut etre etendu
 * et ici par: Corvette, Fregate, Croiseur
 */	

	public abstract String getTypeBateau(); /* declaration qui impose
	 * un model de propriete
	 * permet l'acces type de 
	 * Bateau
	 */	             

	/***  Champps  ***/
	private String nom;
	private int tonnage;	
	EquipageCommande equipage;

	/*** constructors  ***/
	public Bateau(String nom, int tonnage, EquipageCommande equipage) {
		super();
		this.nom = nom;
		this.tonnage = tonnage;
		this.equipage = equipage;
	}

	/*****************  Methods  *******************/
	/***  Getters  ***/  
	/* que des getteurs sur nom et tonnage 
	 * car �tant en lecture seul
	 */
	public String getNom() {   
		return nom;
	}                           
	public int getTonnage() {
		return tonnage;
	}

	/***  Getters and setter d'equipage ***/ 
	public EquipageCommande getEquipage() {
		return equipage;
	}

	public void setEquipage(EquipageCommande equipage) {
		this.equipage = equipage;
	}

	/***  method equals  ***/  
	public boolean equals(String nomBat) {
		if(this.nom == nomBat){
			return true;
		}
		return false;
	}

	/***  method toString  ***/ 
	public String toString() {
		return "Bateau "+" de Type " + this.getTypeBateau()
				+"[nom = " + nom + ", tonnage = " + tonnage +",\n"
				+ " Equipage = "/*+ equipage.getCommandant() */+" "
				+   equipage.toString()
				+ "]";
	}

}
