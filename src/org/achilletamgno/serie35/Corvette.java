package org.achilletamgno.serie35;

public class Corvette extends Bateau {
	private String covette= "Covette";

	public Corvette(String nom, int tonnage, 
			EquipageCommande equipage) {

		super(nom, tonnage, equipage);
		this.covette = "Covette";
	}

	public String getTypeBateau() {
		return covette;
	}

}
