package org.achilletamgno.serie35;

public class Croiseur extends Bateau {
	private String croiseur= "Croiseur";

	public Croiseur(String nom, int tonnage, 
			EquipageCommande equipage) {

		super(nom, tonnage, equipage);
		this.croiseur = "Croiseur";
	}

	public String getTypeBateau() {
		return croiseur;
	}
}
