package org.achilletamgno.serie511a;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class SauvegardeFichier {
	
	/********** constructors *********/
	public SauvegardeFichier(){
		
	}
	
	/******************************* Methods ************************************/
	/********************* method lisFichierTexte ********************/

	public static Map<String, Integer> lisFichierTexte(String nomFichier){

		
		/*
		 *  String implemente deja Comparable, les noms sont tries par ordre alphabetique
		 *  TreeMap <Mot,Score>
		 */
		
		Map<String,Integer> map = new TreeMap<String, Integer>();

		Reader fr = null;
		try {
			fr = new FileReader(new File(nomFichier));
			BufferedReader br = new BufferedReader(fr);
			String line;
			StringTokenizer st;

			while((line = br.readLine())!= null){
				st = new StringTokenizer(line,"\",\" ");
				while(st.hasMoreTokens()){
					//set.add(st.nextToken().trim());
					String tmp = st.nextToken();
					map.put(tmp, getScore(tmp));
				}

			}
			br.close();
			return map;
			//return set;

		}  catch (IOException e) {
			System.out.println("Erreur : " + e.getMessage());
			e.printStackTrace();
		}
		finally{
			try {
				fr.close();
			} catch (IOException e) {
				System.out.println("Erreur : " + e.getMessage());
				e.printStackTrace();
			}
		}
		return null;
	}

	/*
	 * @param chaine Chaine de caractere pour laquelle on calcule le score
	 * @return int Le score de la chaine de caractere passee en parametre
	 */

	public static int getScore(String chaine){

		int score = 0;
		for (int i=0;i<chaine.length();i++){
			score += chaine.charAt(i)-(int)'A' + 1;
		}
		return score;
	}

}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	