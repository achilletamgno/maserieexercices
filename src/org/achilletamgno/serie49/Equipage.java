package org.achilletamgno.serie49;

import java.util.*;

public class Equipage {


	/* --> 1/ -------------------------------------------------
	 * L�interface de l�API Collection adapt�e pour la r�solution de ce probl�me 
	 * est l�interfaceSortedMap car il faut de l�ordre et en plus recuperer le plus
	 * rapidement possible un Marin � partir de son nom qui peut �tre consid�r� comme
	 *  la cl�.
	 */

	/*
	 * ---> 2/ ------------------------------------------------
	 */
	SortedMap<String,Marin> sMap=new TreeMap<String,Marin>();
	
	/*** method getMarinByName() ***/
	public Marin getMarinByName(String nom)	{
		return sMap.get(nom);
	}
	
	/*** method  isMarinPresent  ***/
	public boolean isMarinPresent(Marin m){
		return sMap.containsValue(m);
	}
	
	/*** method  addMarin()  ***/
	public boolean addMarin(Marin m){
		if (m==null) return false;;
		sMap.put(m.getNom(),m) ; return true;
	}
	
	/*** method  removeMarin()  ***/
	public boolean removeMarin(Marin m){
		if(m==null) return false;
		if (sMap.remove(m.getNom())!=null) return true;
		return false;
	}
	
	/*** method  getNombreMarins()  ***/
	public int getNombreMarins(){
		return sMap.size();
	}
	
	/*** method  getEquipage()  ***/
	public SortedMap<String,Marin> getEquipage(){
		return sMap;
	}
	
	/*** method  addAllEquipage()  ***/
	public void addAllEquipage(Equipage e){
		sMap.putAll(e.sMap);
	}
	
	/*** method  clear()  ***/
	public void clear(){
		sMap.clear();
	}
	public String toString(){
		return sMap.toString();
	}

	/*** method  equals() && hashCode() ***/
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Equipage other = (Equipage) obj;
		return this.sMap.equals(other.sMap);
	}
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.getNombreMarins();
		result = prime * result + sMap.hashCode();
		return result;
	}

}

/* --->3/ ------------------------------
 * L�utilisation de l�API Collection pour impl�menter la classe Equipage r�duit 
 * consid�rablement la omplexit� de programmation de ses m�thodes membres, 
 * de plus nombreuses sont pr�-impl�ment�es. 
 * De plus l�API Collection met � notre disposition une panoplie d�impl�mentations
 * de structures de donn�es nous permettant au mieux de faire le choix selon nos 
 * besoins sur le compromis rapidit�-espace m�moire.
 */


