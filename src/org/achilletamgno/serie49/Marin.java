package org.achilletamgno.serie49;

public class Marin {

	/*** les champs ****/
	private String nom;
	private String prenom;

	/********** les constructeurs **************/
	public Marin(String sonNom, String sonPrenom){
		this.nom    = sonNom;
		this.prenom = sonPrenom;	
		}

	/************ Les getteurs ***********/

	/***  getteur de nom ***/	
	public void setNom(String sonNom){
		this.nom=sonNom;    	
	}

	public String getNom(){
		return this.nom;
	}
	/*** getteur de prenom ***/
	public void setPrenom (String sonPrenom){
		this.prenom = sonPrenom;    	
	}

	public String getPrenom(){
		return this.prenom;
	}

	
	
	/******* Surcharge de la m�thode toString    *******/

	public String toString(){

		return nom + " "+ prenom;
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marin other = (Marin) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}

}
