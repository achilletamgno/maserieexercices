package org.achilletamgno.serie49;

import java.util.Comparator;

public class MarinComparator implements Comparator<Marin>{

	public int compare(Marin m1, Marin m2) {
		if (m1.getNom().equals(m2.getNom())){
			return m1.getPrenom().compareTo(m2.getPrenom());
		}
		return m1.getNom().compareTo(m2.getNom());
	}

}