package org.achilletamgno.serie510a;

public class Palindrome {
	
	public static boolean estPalindrome(long nombre){

		String chaine = Long.toString(nombre);
		for (int i = 0; i <= chaine.length()/2; i++) {
			if (chaine.charAt(i) != chaine.charAt(chaine.length()-1-i)) {
				return false;
			}
		}
		return true;    
	}
}
