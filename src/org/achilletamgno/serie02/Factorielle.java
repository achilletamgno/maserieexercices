package org.achilletamgno.serie02;
import java.math.BigInteger;


/*Class Factorielle*/
public class Factorielle {

	/*** methods pour calcul d'un factoriel ****/
	
	/* factoriel avec le type integer */
	public int factorielle (int n ){

		int fact = 1;
		for (int i=1; i<=n; i++) {
			fact = fact * i;
		}		
		return fact;
	}

	/* factoriel avec le type double */
	public double factorielle (double n ){

		double fact = 1.0d;
		for (int i=1; i<=n; i++) {
			fact = fact * i;
		}		
		return fact;
	}
	
	/* factoriel avec le type BigInteger */
	public BigInteger factorielle (BigInteger n ){

		BigInteger fact=BigInteger.ONE ;
		for (BigInteger index = BigInteger.ONE; 
				index.subtract(n).signum() <=0 ; 
				index = index.add(BigInteger.ONE)){

			fact = fact.multiply(index);
		}		
		return fact;
	}

};
