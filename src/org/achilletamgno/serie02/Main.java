package org.achilletamgno.serie02;

import java.math.BigInteger;


public class Main {
	
	public static void main(String[] args) {
	
		/* creation d'un objet factorielle nomme fact */
	  Factorielle fact = new Factorielle();
	  
	  /* calcul du factoriel avec le type integer */
	  int k=1;
	  for(int i=0; i<=15 ; i++) {
	  System.out.println("\n factoriel de "+k +" = " + fact.factorielle(k++)); 
	  }
	  
	  /*long l=1;
	  for(int i=0; i<=16 ; i++) {
	  //System.out.println("valeurs integers");
	  System.out.println("\n factoriel de "+l +" = " + fact.factorielle(l++)); 
	  }*/
	  
	  /* calcul du factoriel avec le type double */
	  double d=1.0d;
	  for(int i=0; i<=16 ; i++) {
	  //System.out.println("valeurs integers");
	  System.out.println("\n factoriel de "+d +" = " + fact.factorielle(d++)); 
	  } 
	  
	  /* calcul du factoriel avec le type BigInteger */
	  BigInteger b = new BigInteger("100");
	  System.out.println(b + " = " +fact.factorielle(b));
	  
	}
}
