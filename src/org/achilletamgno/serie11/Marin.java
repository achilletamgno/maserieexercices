package org.achilletamgno.serie11;

public class Marin {

	/*** les champs ****/
	private String nom;
	private String prenom;
	private int    salaire;


	/********** les constructeurs *********/
	public Marin(String nom, String prenom, int salaire){

		this.nom     = nom;
		this.prenom   = prenom;
		this.salaire = salaire;
	}

	public Marin(String nom, int salaire){

		this(nom ," ",salaire); /* utise directement le constructeur 
		                           Marin(String nom, String prenom, int salaire) */
	}

	

	/************ Les getteurs ***********/

	/***  getteur de nom ***/	
	public void setNom(String sonNom){
		this.nom=sonNom;    	
	}

	public String getNom(){
		return this.nom;
	}
	/*** getteur de prenom ***/
	public void setPrenom (String sonPrenom){
		this.prenom = sonPrenom;    	
	}

	public String getPrenom(){
		return this.prenom;
	}

	/*** getteur de salaire ***/
	public void setSalaire (int sonSalaire){
		this.salaire = sonSalaire;    	
	}

	public int getSalaire(){
		return this.salaire;
	}

	/********* les methodes **********/
	public void augmenteSalaire(int augmentation){
		this.salaire += augmentation; 
	}

	/******* Surcharge de la m�thode toString    *******/

	public String toString(){

		return nom /*+" "+ prenom */+" : "+ salaire;
	}

/**** Modification de la class Marin et Surcharge deequal ****/
	
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		Marin other = (Marin) obj;
		if (nom == null) {
			if (other.nom != null){
				return false;
			}
		} else if (!nom.equals(other.nom)){
			return false;
		}
		if (prenom == null) {
			if (other.prenom != null){
				return false;
			}
		} else if (!prenom.equals(other.prenom)){
			return false;
		}
		if (salaire != other.salaire){
			return false;
		}
		return true;
	}
	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + salaire;
		return result;
	}

}
