package org.achilletamgno.serie48;

import java.util.SortedSet;
import java.util.TreeSet;


public class Main {
	
	public static void main(String[] args) {

		SortedSet<Marin> set = new TreeSet<Marin>(new MarinComparator());
		SortedSet<Marin> sSet = new TreeSet<Marin>();

		Marin m1 = new Marin("Alain", "Colas");
		//Marin m2 = new Marin("Alain", "Colas");
		Marin m2 = new Marin("Patrice", "Carpentier");
		Marin m3 = new Marin("Jacques", "Cassard");	
		/*Marin m4 = new Marin("Nicolas", "Baudin");
		Marin m5 = new Marin("J�r�mie", "Beyou");
		Marin m6 = new Marin("Alain", "Bombard");*/
	
		ComparableMarin m4 = new ComparableMarin("Jacques", "Cassard");
		ComparableMarin m5 = new ComparableMarin("Nicolas", "Baudin");
		ComparableMarin m6 = new ComparableMarin("Alain", "Bombard");


		set.add(m2);
		set.add(m1);
		set.add(m3);
	
		System.out.println(set);

		sSet.add(m5);
		sSet.add(m6);
		sSet.add(m4);


		System.out.println(sSet);

	}

}
