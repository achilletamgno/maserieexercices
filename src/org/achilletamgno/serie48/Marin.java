package org.achilletamgno.serie48;

public class Marin {

	/*** les champs ****/
	private String nom;
	private String prenom;

	/********** constructors **************/
	public Marin(String sonNom, String sonPrenom){
		this.nom    = sonNom;
		this.prenom = sonPrenom;	
		}

	/************ getters ***********/

	/***  getter de nom ***/	
	public void setNom(String sonNom){
		this.nom=sonNom;    	
	}

	public String getNom(){
		return this.nom;
	}
	/*** getter de prenom ***/
	public void setPrenom (String sonPrenom){
		this.prenom = sonPrenom;    	
	}

	public String getPrenom(){
		return this.prenom;
	}

	
	
	/******* Surcharge  method toString    *******/

	/*public String toString(){

		return nom + " "+ prenom;
	}*/
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(nom).append(" ").append(prenom);
		return  sb.toString();
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marin other = (Marin) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}

}
