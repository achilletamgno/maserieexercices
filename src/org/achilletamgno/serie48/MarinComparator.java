package org.achilletamgno.serie48;

import java.util.Comparator;

public class MarinComparator implements Comparator<Marin>{

	public int compare(Marin m1,Marin m2){
		int c=m1.getNom().compareTo(m2.getNom());
		if(c==0) { 
			return m1.getPrenom().compareTo(m2.getPrenom());
		}
		return c;

     }
}