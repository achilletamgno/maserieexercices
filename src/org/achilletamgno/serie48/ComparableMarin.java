package org.achilletamgno.serie48;

public class ComparableMarin extends Marin implements Comparable<Marin> {

	public ComparableMarin(String sonNom, String sonPrenom) {
		super(sonNom, sonPrenom);
	}
	
	public int compareTo(Marin m) {

		if (this.getNom().equals(m.getNom())){
			return this.getPrenom().compareTo(m.getPrenom());
		}
		return this.getNom().compareTo(m.getNom());
	}

}
