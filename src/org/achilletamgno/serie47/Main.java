package org.achilletamgno.serie47;

import java.util.HashSet;

public class Main {

	public static void main(String[] args) {

		Marin m1 = new Marin("Alain", "Colas");
		Marin m2 = new Marin("Alain", "Colas");
		Marin m3 = new Marin("Patrice", "Carpentier");
		/*Marin m3 = new Marin("Jacques", "Cassard");	
		Marin m4 = new Marin("Nicolas", "Baudin");
		Marin m5 = new Marin("J�r�mie", "Beyou");
		Marin m6 = new Marin("Alain", "Bombard");*/

		/*
		 * La s�mantique du Set : un Set ne comporte pas de doublon.
         * Sa Classe d�impl�mentation est HashSet.
		 */

		System.out.println(m1.equals(m2)); // true
		System.out.println(m1.equals(m3)); // false

		HashSet<Marin> set = new HashSet<Marin>();
		
		/*
		 * L'ajout s'effectue correctement � l'absence de hashCode et equals.
		 * Cela est du au fait que HashSet utilise ces deux fonctions pour comparer
		 * le hashCode de l'element a ajouter avec ceux des elements deja presents 
		 * dans la liste
		 */
	
		System.out.println(m2 + " Ajoute ? : " + set.add(m2)); /* true*/
		
		System.out.println(m1 + " Ajoute ? : " + set.add(m1)); /* false */
		
		System.out.println(m3 + " Ajoute ? : " + set.add(m3)); /* true */
			
		System.out.println(set);
	}

}
