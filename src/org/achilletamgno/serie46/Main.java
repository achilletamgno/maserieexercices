package org.achilletamgno.serie46;

import java.util.*;


//import org.achilletamgno.serie34.Marin;

public class Main {

	public static void main(String[] args) {
		
		
		Marin m1 = new Marin("Alain", "Colas");
		Marin m2 = new Marin("Patrice", "Carpentier");
		Marin m3 = new Marin("Jacques", "Cassard");	
		Marin m4 = new Marin("Nicolas", "Baudin");
		Marin m5 = new Marin("J�r�mie", "Beyou");
		Marin m6 = new Marin("Alain", "Bombard");
		//Marin m7 = new Marin("Franck","Cammas");
		
		System.out.println("------------------------------------\n");

		Marin[] tabMarins1 = {m1,m2,m3};
		Marin[] tabMarins2 = {m4,m5,m6,m1};

		

		ArrayList<Marin> marinsListA = new ArrayList<Marin>();
		ArrayList<Marin> marinsListB = new ArrayList<Marin>();
		/*
		 * Ajout de 4 marins a la liste
		 */
		for (Marin m : tabMarins1){
			marinsListA.add(m);
		}
		
		for (Marin m : tabMarins2){
			marinsListB.add(m);
		}
		
		Equipage equipageA = new Equipage(marinsListA);
		Equipage equipageB = new Equipage(marinsListB);
		Equipage equipageC = new Equipage(marinsListA);

		/*
		 * Affichage des 4 marins ajoutes
		 */
		System.out.println("Equipage A : "+ equipageA);
		
		
		/*
		 * Ajout d'un marin existant
		 */
		
		System.out.println(" Ajout de " + m1 +" a l'equipage A : " + equipageA.addMarin(m1));

		System.out.println("Equipage A : "+ equipageA);
		
		System.out.println(" Suppression de " + m1 +" de l'Equipage A : " + equipageA.removeMarin(m1));

		System.out.println("Equipe A : "+ equipageA);
		
		/*
		 * Ajout d'une liste a une autre liste
		 */
		
		
		
		System.out.println("------------------------------------\n");
		equipageA.addMarin(m1);
		System.out.println("Equipage A : "+ equipageA);
		System.out.println("Equipage B : "+ equipageB);
		System.out.println("Ajout de toute l'Equipage B a l'Equipage A : "
		                    +equipageA.addAllEquipage(equipageB));
		/* ex : false , Equipe B contient le marin m2 qui est deja un marin de l'Equipe A */
		
		
		System.out.println("Equipage A : "+equipageA);
		
		
		/*
		 * Equipe A et C contient les meme marins, mais pas dans le meme ordre !
		 */
		
		System.out.println("------------------------------------\n");
		System.out.println("Equipage A : " + equipageA + " HashCode : "+ equipageA.hashCode());
		System.out.println("Equipage C : " + equipageC+ " HashCode : "+ equipageC.hashCode());
		
		System.out.println("------------------------------------\n");
		System.out.println("Equipage A  et Equipe C sont-ils egaux ? : " + equipageA.equals(equipageC));
		
		System.out.println("------------------------------------\n");
		
		System.out.println("Equipage A : "+ equipageA);
		System.out.println("Effacement de l'Equipage A ");
		equipageA.clear();
		System.out.println("Equipage A : "+ equipageA);
		
	}

}
