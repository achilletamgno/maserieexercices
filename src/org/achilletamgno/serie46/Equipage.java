package org.achilletamgno.serie46;

import java.util.*;

public class Equipage {

	private ArrayList<Marin> list = new ArrayList<Marin>();

	/*********** constructors ******************/
	public Equipage(){

	}

	public Equipage(ArrayList<Marin> marinsList) {
		for (Marin marin : marinsList){
			list.add(marin);
		}
	}

	/************************************ Methods ************************************/

	/**** Method addMarin ****/
	public boolean addMarin(Marin marin){
		if (!list.contains(marin)){
			list.add(marin);
			return true;
		}
		return false; 
	}

	/**** Method toString() ****/
	public String toString() {
		return  list.toString();
	}

	/**** Method removeMarin **/
	public boolean removeMarin(Marin marin){
		return this.list.remove(marin);
	}

	/*********** Method addAllEquipage ***********/
	public boolean addAllEquipage(Equipage eq){

		/*
		 *  AddAll n'echoue pas dans ArrayList :
		 *  Il FAUT v�rifier si les deux listes sont disjointes 
		 *  pour eviter les doublons
		 */

		if (Collections.disjoint(this.list, eq.list)){
			return this.list.addAll(eq.list);
		}	
		return false;
	}


	/**** Method clear() *****/
	public void clear(){
		this.list.clear();
	}


	/***********************   Method equals & hashCode  *****************************/
	public boolean equals(Object obj) {
		if (!(obj instanceof Equipage)){
			return false;
		}

		Equipage other = (Equipage)obj;


		/*
		 * la method equals de list ne marche pas 
		 * 
		 *  Les deux listes doivent avoir les memes objets pour que �a marche
		 *  et on ne tient pas compte de l'ordre
		 */


		return (this.list.containsAll(other.list)&& other.list.containsAll(this.list));
	}

	public int hashCode(){


		/*
		 * Tri de la liste des marins en fonction des hashCodes
		 * leurs egalit� au sens de equals
		 */


		Collections.sort(this.list,new Comparator<Marin>(){
			public int compare(Marin m1, Marin  m2){
				return m1.hashCode() - m2.hashCode();
			}
		});

		final int prime = 31;
		int result = 1;
		result = prime * result + ((list == null) ? 0 : list.hashCode());
		return result;
	}


}



