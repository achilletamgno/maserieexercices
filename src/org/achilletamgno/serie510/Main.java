package org.achilletamgno.serie510;

import java.io.IOException;
import java.util.List;

public class Main {	

	public static void main(String[] args)throws IOException{
	
		Marin marinTestNull = null;    // test pour marin null
		
		Marin m0 = new Marin("Surcouf","Robert",1123);
		Marin m1 = new Marin("Nicolas","Baudin",1000);
		Marin m2 = new Marin("Hippolyte","de Bouchard",500);
		Marin m3 = new Marin("Nicolas","Baudin",700);
		Marin m4 = new Marin("Nicolas","Baudin",9000);
		Marin m5 = new Marin("Nicolas","Baudin",10000);
	
		/*
		 * Notre tableau de marins � utiliser dans sauveObjet 
		 */
		Marin[] tabMarins= new Marin[5];
		tabMarins[0]=m1;
		tabMarins[1]=m2;
		tabMarins[2]=m3;
		tabMarins[3]=m4;
		tabMarins[4]=m5;
		
		
        /*
         *  nos fichiers
         */
		String nomFichierTxt = "files/marins.txt";
		String nomFichierBin = "files/marins.bin";
		String nomFichierBinObj = "files/marinsObjets.bin";
		
		
		
		
		/*
		 * debut de sauvegarde
		 */

		System.out.println("----------- sauveFichierText if null ------------- \n");
		Sauvegarde.sauveFichierText(nomFichierTxt, marinTestNull); 
		
		System.out.println("----------- sauveFichierText ----------- \n");
		Sauvegarde.sauveFichierText(nomFichierTxt, m0);
		Sauvegarde.sauveFichierText(nomFichierTxt, m1);
		Sauvegarde.sauveFichierText(nomFichierTxt, m2);
		Sauvegarde.sauveFichierText(nomFichierTxt, m3);
		Sauvegarde.sauveFichierText(nomFichierTxt, m4);
		Sauvegarde.sauveFichierText(nomFichierTxt, m5);
		
		
		System.out.println("----------- lisFichierText ----------- \n");
		
		List<Marin> marinsLus = Sauvegarde.lisFichierTexte(nomFichierTxt);
		for (Marin m : marinsLus){
			System.out.println(m);
		}
		
        	
		
		System.out.println("----------- sauveChampBinaire -----------\n");
		
		Sauvegarde.sauveChampBinaire(nomFichierBin, m0);
		Sauvegarde.sauveChampBinaire(nomFichierBin, m1);
		Sauvegarde.sauveChampBinaire(nomFichierBin, m2);

		
		System.out.println("----------- lisChampBinaire -----------\n");
		
		List<Marin> marinsBinaireLus = Sauvegarde.lisChampBinaire(nomFichierBin);
		for (Marin m : marinsBinaireLus){
			System.out.println(m);
		}
		

		
		System.out.println("----------- sauveObjet -----------\n");
		
		
		Sauvegarde.sauveObjet(nomFichierBinObj, tabMarins);
		
		System.out.println("----------- lisObjet -----------\n");
		
		List<Marin> marins2 = Sauvegarde.lisObjet(nomFichierBinObj);
		
		for(Marin m : marins2 ){
			System.out.println(m);
		}
		
	}
	
	/* 
	 * Au premier test apr�s avoir vid� les fichiers,
	 * 
	 * - Taille du FichierTexteMarins   = 128 bytes
	 * - Taille du FichierBinaireMarins = 126 bytes
	 * - Taille du Taille du FichierObjetMarins = 223 bytes
	 * 
	 * --> 4/ Taille du FichierBinaireMarins < Taille du FichierTexteMarins
	 *                                             < Taille du FichierObjetMarins
	 *                                             
	 *     donc la fa�on la plus efficace de sauvegarder nos marin c'est en binaire
	 */
	
				
}
		

		
		







