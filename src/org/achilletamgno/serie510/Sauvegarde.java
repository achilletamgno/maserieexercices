package org.achilletamgno.serie510;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Sauvegarde {

	/********** constructors *********/
	public Sauvegarde(){
	}

	/******************************* Methods ************************************/

	/********************* method sauveFichierText ********************/
	public static void sauveFichierText(String nomFichier, Marin marin) {

		//d�claration notre objet d'ecriture en dehors du bloc try/catch
		Writer writer = null;

		if(marin == null){
			System.err.println(" ??????? le marin � sauvegarder est null \n");
			return;
		}
		try {
			//On instancie notre objet :
			writer = new FileWriter(new File(nomFichier),true);


			StringBuilder sb = new StringBuilder(marin.getNom()).append('|')
					.append(marin.getPrenom()).append('|')
					.append(marin.getSalaire()).append("\n");

			// permet d'afficher les lignes de marin
			writer.write(sb.toString());


		} catch (IOException e){
			// gestion  des erreurs de la pile d'appel
			System.out.println("Erreur : " + e.getMessage());

			// gestion  des erreurs de la pile d'appel
			e.printStackTrace();
		}

		finally{
			// il se peut que l'ouverture du flux � echou�
			// et que le write ne soit pas initialis�
			if(writer != null){
				try{
					// fermeture du flux
					writer.close();
				} catch(IOException e){
					System.out.println("Erreur : " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}


	/********************* method lisFichierTexte ********************/
	public static List<Marin> lisFichierTexte(String nomFichier){
		Reader fr = null;
		List<Marin> marins = new ArrayList<Marin>();
		try {
			//fr = new FileReader(nomFichier);		
			BufferedReader br = new  BufferedReader(fr = new FileReader (
					new File(nomFichier)));

			String line = null;

			while((line = br.readLine())!= null){

				/*
				 *  le r�le de StringTokeniser() est de  retourner le 
				 *  un String apr�s le token(jeton) que tu auras pass� en param�tre
				 */	

				StringTokenizer st = new StringTokenizer(line, "|");

				String nom    = st.nextToken();
				String prenom = st.nextToken();
				int    salaire= Integer.parseInt(st.nextToken());

				marins.add(new Marin(nom, prenom, salaire));
			}
			br.close();
			return marins;

		}  catch (IOException e) {
			// gestion  des erreurs de la pile d'appel
			System.out.println("Erreur : " + e.getMessage());
			e.printStackTrace();
		}
		finally{
			try {
				// fermeture du flux
				fr.close();
			} catch (IOException e) {
				System.out.println("Erreur : " + e.getMessage());
				e.printStackTrace();
			}
		}
		return null;
	}


	/*
	 * --->2a/ pour les types primitifs le flux � utiliser est les DataOutputStream
	 * 
	 * ********************* method sauveChampBinaire *******************
	 */


	public static void sauveChampBinaire(String nomFichier, Marin marin){

		DataOutputStream dos = null;
		FileOutputStream fos = null;

		if(marin == null){
			System.err.println(" ??????? le marin � sauvegarder est null \n");
			return;
		}
		try {
			/*
			 * --> dos permet d'ecrire les types primitifs Java 
			 * (transforme les types primitif en binaire)
			 * 
			 * --> fos permet recup�re les flux binaire
			 * (recup�re et les insert dans les fichiers)
			 */
			fos = new FileOutputStream(new File(nomFichier), true);
			dos = new DataOutputStream(fos);

			dos.writeInt(marin.getNom().length());
			dos.writeChars(marin.getNom());

			dos.writeInt(marin.getPrenom().length());
			dos.writeChars(marin.getPrenom());

			dos.writeInt(marin.getSalaire());

		}  catch (IOException e) {
			// gestion  des erreurs de la pile d'appel
			System.out.println("Erreur : " + e.getMessage());
			e.printStackTrace();
		}
		finally{
			// il se peut que l'ouverture du flux � echou�
			// et que le write ne soit pas initialis�
			if (fos != null){	
				try{
					// fermeture du flux
					fos.close();					
				}catch(IOException e){
					System.out.println("Erreur : " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}



	/********************* method lisChampBinaire ********************/
	public static List<Marin> lisChampBinaire(String nomFichier){

		List<Marin> marins =new ArrayList<Marin>();
		DataInputStream dis =null;

		try{
			InputStream fis= new FileInputStream(nomFichier);
			dis=new DataInputStream(fis);
			char [] chaine=new char [1024];
			String nom,prenom;
			int tempInt;
			
			while (true){
				tempInt=dis.readInt();
				for(int i=0;i<tempInt; ++i){
					chaine[i]=dis.readChar();
				}
				nom=new String (chaine, 0, tempInt);
				tempInt=dis.readInt();
				for(int j=0;j<tempInt; ++j){
					chaine[j]=dis.readChar();
				}
				prenom=new String(chaine, 0, tempInt);
				tempInt=dis.readInt();
				if (marins.add(new Marin(nom,prenom,tempInt))== false){
					System.err.println("Echec de l'ajout du Marin dans la liste");
					return marins;
				}
			}

		}  catch (EOFException eofe) {
			return marins;
		}
		catch (IOException ioe) {
			System.out.println("Erreur"+ioe.getMessage());
			ioe.printStackTrace();
		}
		finally{
			if(dis !=null)
				try{
					dis.close();
				}catch (IOException ioe) {
					System.out.println("Erreur"+ioe.getMessage());
					ioe.printStackTrace();
				}
		}
		return marins;
	}





	/* --> 3a/ Le flux permettant d��crire les objets dans un fichier 
	 *         binaire est ObjetOuputStream.
	 * 
	 * ******************** method sauveObjet **********************
	 * 
	 */
	public static void sauveObjet(String fichier, Marin[] marins) {
		if (marins == null){
			System.err.println("Impossible d'ecrire une liste de marins");
			return ;
		}
		ObjectOutputStream oos=null;
		try{
			OutputStream fos =new FileOutputStream(fichier);
			oos=new ObjectOutputStream(fos);
			for(Marin m:marins){
				oos.writeObject(m);
			}
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
		finally{
			if(oos !=null)
				try{
					oos.close();
				}catch (IOException ioe) {
					System.out.println("Erreur"+ioe.getMessage());
					ioe.printStackTrace();
				}
		}
	}


	/************************ method lisObjet ***************************/
	public static List<Marin> lisObjet(String fichier){
		
		List<Marin> marins = new ArrayList<Marin>();
		ObjectInputStream ois = null;
		
		try{
			FileInputStream fis = new FileInputStream(fichier);
			ois = new ObjectInputStream(fis);
			Marin marin = null;
			do{
				marin=(Marin) ois.readObject();
				if(marin == null) continue ;
				if(marins.add(marin)== false){
					System.err.println("Echec de l'ajout � la liste de marins");
					return null;
				}
			}
			while(true);
		}
		catch(EOFException eofe){
			System.out.println("Fin fichier atteinte");
		}
		catch(IOException ioe){
			System.out.println("Erreur"+ ioe.getMessage());
			ioe.printStackTrace();
		}
		catch(ClassNotFoundException cnfe){
			System.out.println("Erreur"+cnfe.getMessage());
			cnfe.printStackTrace();
		}finally{
			if(ois !=null){
				try{
					ois.close();
				}catch(IOException ioe){
					System.out.println("Erreur"+ioe.getMessage());
					ioe.printStackTrace();
				}
			}
		}
		return marins;
	}
}

