package org.achilletamgno.serie12;
import java.util.Arrays;


public class MarinUtil {
	
	/** Les champs MarinUtil **/ 
	public float[] tableauSalaireMax;
	public float[] tableauSalaireMedian;


	/*** les methodes de la classe MarinUtil ***/
	
	
	/** Augmentation de salaire au tableau de salaire pass� en param�tre */
	public void augmenterSalaire(Marin[]  marins, int pourcentage){
		for(int i=0; i<= marins.length; i++){
			marins[i].augmenteSalaire(((marins[i].getSalaire())*pourcentage)/100);
			System.out.println("Marin  " + (i+1) +  marins[i]  );
		}
	}

	/**getMaxSalaire Pour obtenir le salaire Max du tableau des 5**/
	public float getSalaireMax (Marin[] marins){
		tableauSalaireMax = new float[marins.length];
		for(int i=0;i<marins.length;i++){
			tableauSalaireMax[i]=marins[i].getSalaire();
			Arrays.sort(tableauSalaireMax);
		}
		
		return  tableauSalaireMax[tableauSalaireMax.length-1];
	}

	/** getMoyenneSalaire pour avoir la moyenne des salaires**/
	public float getMoyenneSalaire(Marin[] marins){
		float Moyenne = 0;
		for(int i=0;i<5;i++){
			Moyenne += (marins[i].getSalaire());
		}
		Moyenne /= 5;
		System.out.println(" le salaire Moyen du tableau est: " );
		return Moyenne;
	}
	
   /** getMedianeSalaire nous retourne la m�diane des salaires du tableau**/
	public float getMedianeSalaire(Marin[] marins){
		tableauSalaireMedian=new float[marins.length];
		for(int i=0; i<marins.length; i++){
			tableauSalaireMedian[i]= marins[i].getSalaire();
		}
		Arrays.sort(tableauSalaireMedian);
		System.out.println("Salaire Median est: " );
		return tableauSalaireMedian[3];
	}

}
