package org.achilletamgno.serie613;

public class Movie {

		private String title;
		private Category category;
		private PriceCode priceCode;

		public Movie(){
		}
		public Movie(String title,Category category,PriceCode priceCode){
			this.title = title;
			this.category = category;
			this.priceCode = priceCode;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public Category getCategory() {
			return category;
		}
		public void setCategory(Category category) {
			this.category = category;
		}
		public PriceCode getPriceCode() {
			return priceCode;
		}
		public void setPriceCode(PriceCode priceCode) {
			this.priceCode = priceCode;
		}

		public float getPrice(int jours){

			float prix=0;

			switch (priceCode) {

			case NOUVEAUTE:
				prix = jours;
				break;

				//
				// Pour NORMAL et ENFANT : On suppose qu'on ne peut pas louer un film pour
				// une duree inferieure au minimum (2 jrs pour NORMAL et 3 jrs pour ENFANT)
				//
			case NORMAL:
				if (jours==2){
					prix = jours;
				}else{
					prix = 2 + (jours-2)*1.5f;
				}
				break;
			case ENFANT:
				if (jours==3){
					prix = 1.5f;
				}else{
					prix = 1.5f + (jours-3)*1.5f;
				}
			}
			return prix;
		}


		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((title == null) ? 0 : title.hashCode());
			return result;
		}

		public boolean equals(Object obj) {
			if (!(obj instanceof Movie)){
				return false;
			}

			Movie other = (Movie)obj;
			return  title.equals(other.title); // Deux films sont egaux s'il ont le meme titre
		}


		public String toString() {
			StringBuffer sb = new StringBuffer();
			return  sb.append(title).append(" : ").append(category).append(" : ").append(priceCode).toString();
		}

	}

