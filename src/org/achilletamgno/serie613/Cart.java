package org.achilletamgno.serie613;

import java.util.HashMap;
import java.util.Map;

public class Cart {
	
	private Map<Movie, Integer> map = new HashMap<Movie, Integer>();

	/*** constructor ****/
	public Cart(){
	}

	/**
	 * Ajoute le film avec le nombre de jours de location au panier
	 * @param movie Le film a jouter au panier pour la location
	 * @param jours Nombre de jours de location 
	 */
	
	/*********************************************** Methods ************************************/
	/*** method addMovie ****/
	public void addMovie(Movie movie, int jours){
		//
		// On aditionne le nombre de jours de location pour tout film loue plusieurs fois
		//
		if (map.containsKey(movie)){
			map.put(movie, map.get(movie) + jours);
		}else{
			
			// Premier location du film
			map.put(movie, jours);
		}
	}

	/**
	 * Retire du panier le film passe en parametre
	 * @param movie le film a retirer du panier
	 */
	
	/*** method removeMovie ****/
	public void removeMovie(Movie movie){
		// retourne la valeur correspondante au film a supprimer s'il existe
		// null sinon
		map.remove(movie);
	}

	/**
	 * Calcul le prix total du panier
	 * @return Somme total du panier
	 */

	/*** method getPrice ****/
	public float getPrice(){
		float prix = 0;
		for (Map.Entry<Movie, Integer> entry : map.entrySet()){
			prix += entry.getKey().getPrice(entry.getValue());
		}
		return prix;
	}


	/**
	 * Calcul des points de fidelite accumules dans ce panier en fonction
	 * de nombre de jours de location de chaque film
	 * 
	 * @return Nombre de points de fidelite accumules
	 */
	
	/*** method getFidelityPoints ****/
	public float getFidelityPoints(){
		float points=0,tmp=0;

		for (Map.Entry<Movie, Integer> entry : map.entrySet()){

			Movie  movie = entry.getKey();
			Integer jours = entry.getValue();


			switch(movie.getPriceCode()){

			case ENFANT:
				points = 1.5f;
				if (jours>3){
					points = points + (jours-3)*1.5f;
				}
				break;

			case NORMAL:
				points = 1;
				if (jours>2){
					points = points + 1.5f*(jours - 2);
				}
				break;

			case NOUVEAUTE:
				points = 3*jours;
				break;
			}
			tmp+=points;
		}
		return tmp;
	}
	
	
	/*** method toString() ****/
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Map.Entry<Movie, Integer> entry : map.entrySet()){
			sb.append(entry.getKey()).append(" : ").append(entry.getValue()).append(" jour(s) \n");
		}
		return sb.toString();
	}
}
