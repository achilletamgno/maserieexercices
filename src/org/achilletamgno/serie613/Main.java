package org.achilletamgno.serie613;

public class Main {

	public static void main(String[] args) {

		Movie film = new Movie("La fin du Monde",Category.DRAME,PriceCode.NORMAL);
		Movie film2 = new Movie("Batman",Category.COMEDIE,PriceCode.NOUVEAUTE);
		Movie film3 = new Movie("DJANGO",Category.AVENTURE,PriceCode.ENFANT);

		Cart cart = new Cart();

		
		cart.addMovie(film, 3);
		cart.addMovie(film2, 12);
		cart.addMovie(film3, 6);
		cart.addMovie(film3, 4);

		//		System.out.println(film);
		System.out.println("\n------------------------------------------------------------");
		System.out.println(cart);
		
		cart.removeMovie(film);
		System.out.println(cart);
		
		cart.addMovie(film, 3);
		System.out.println(cart);
		
		StringBuffer sb = new StringBuffer();
		sb.append("Total : ").append(cart.getPrice()).append("�").append("\t").append("Points fidelite : ").append(cart.getFidelityPoints()).append(" points");
		System.out.println(sb);

	}

}
