package org.achilletamgno.serie33;

///import org.achilletamgno.serie33.Capitaine.grade;


public class LeMain {

	public static void main(String[] args) {
	
		Marin m1 = new Marin("Nicolas", "Baudin", 1000);
		Marin m2 = new Marin("J�r�mie", "Beyou", 3000);
		Marin m3 = new Marin("Alain", "Bombard", 8000);
		Marin m4 = new Marin("Franck","Cammas",4000);
		//Marin m5 = new Marin("Patrice", "Carpentier", 1000);
		Capitaine cap1 = new Capitaine( " Jack " ," Sparrow ", 10000, Grade.grade.CAPITAINE);
		//Capitaine cap2 = new Capitaine( " Davy " ,"  Jones ", 60000, grade.AMIRALE);
		
		
		Equipage equipage  = new Equipage();
		
		equipage.addMarin(cap1);
		//equipage.addMarin(cap2);
		equipage.addMarin(m1);
		equipage.addMarin(m2);
		equipage.addMarin(m3);
		equipage.addMarin(m4);
		
		      
		/*
		 * Oui il est possible de ranger un capitain dans un �quipage sans modifier 
		 * la classe Equipage car un capitaine est avant tout un marin 
		 * 
		 * */
		
		System.out.println(equipage.toString() + "\n");
		

	}

}
