package org.achilletamgno.serie33;

import org.achilletamgno.serie33.Grade.grade;

public class Capitaine extends Marin {


	/**  Chanps Grade **/
	private grade leGrade;

	/* Constructeur */ 
	public Capitaine(String nom, String prenom, int salaire, grade legrade) {
		super(nom, prenom, salaire);
		this.leGrade = legrade;
	}

	/** Sucharge de la Methode toString **/
	public String toString() {
		return "\n"+"Capitaine [leGrade = " + leGrade + ", nom = " + getNom() + ", prenom = " 
	                        + getPrenom() + ", salaire = " + getSalaire() + "  ]"+ "\n";
	}


	/******* getteur et setteur sur grade ******/
	public grade getLeGrade() {
		return leGrade;
	}

	public void setLeGrade(grade leGrade) {
		this.leGrade = leGrade;
	}

	/** Sucharge de la Methode hashCode **/
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((leGrade == null) ? 0 : leGrade.hashCode());
		return result;
	}


	/** Sucharge de la Methode equals **/
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Capitaine))
			return false;
		Capitaine other = (Capitaine) obj;
		if (leGrade != other.leGrade)
			return false;
		return true;
	}




}
