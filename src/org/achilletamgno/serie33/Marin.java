package org.achilletamgno.serie33;

public class Marin {

	/*** les champs ****/
	private  String nom;      /*  Nom et Prenom n'ont que         */ 
	private  String prenom;   /*  des getteurs mais pas de setteur*/
	private  int    salaire;   
	/** salaire � un getteur et un setteur
	 *  car il peut �tre modifi� une fois 
	 *  l'instance cr�ee 
	 **/


	/********** les constructeurs *********/
	public Marin(String nom, String prenom, int salaire){

		this.nom     = nom;
		this.prenom   = prenom;
		this.salaire = salaire;
	}



	/************************* Les Methodes ********************************/

	/******* Surcharge de la m�thode toString    *******/

	public String toString() {
		return "Marin [nom = " + nom + ", prenom = " + prenom + ", salaire = "
				+ salaire + "]"+ "\n";
	}


	/******* getteur sur nom et prenom ******/

	public String getNom() {
		return nom;
	}


	public String getPrenom() {
		return prenom;
	}

	/******* getteur et setteur salaire     *******/
	public int getSalaire() {
		return salaire;
	}

	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}


	/******* Surcharge des m�thode hashCode() et equals()    *******/
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + salaire;
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Marin))
			return false;
		Marin other = (Marin) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (salaire != other.salaire)
			return false;
		return true;
	}
	
	

};













