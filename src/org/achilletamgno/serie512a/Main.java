package org.achilletamgno.serie512a;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) {
		int nblignes =0, nbMots = 0,nbIlya=0;
		int nbMotsSixLettres = 0,nbTirets = 0,nbMotsTroisLettres=0;
		String nomFichier = new String("file12a/germinal.txt");


		/*
		 *  map : HashMap de tous les mots et leurs nombres d'occurance dans le texte
		 */

		Map<String, Integer> map = new HashMap<String, Integer>();


		/*
		 *  histMap : l'histogramme
		 */

		Map<Integer, Integer> histMap = new HashMap<Integer, Integer>();


		/*
		 *  Entry pour reperer le mot le plus long 
		 */

		Entry<Integer,Integer> maxEntry = null;

		Reader fr = null;
		StringTokenizer st = null;

		try {
			fr = new FileReader(new File(nomFichier));
			BufferedReader br = new BufferedReader(fr);
			LineNumberReader lnr = new LineNumberReader(br);
			String line;

			/*
			 *  Lecture ligne pas ligne
			 */

			while((line = lnr.readLine())!= null){


				/*
				 * Remet les ligne en minuscule
				 */

				line = line.toLowerCase();


				/*
				 *  Compte le nombre de ligne commencant par un tiret
				 */

				if (line.startsWith("-")){
					nbTirets++;
				}


				/*
				 *  Le numero de la dernier ligne 
				 */

				nblignes = lnr.getLineNumber();


				/*
				 *  Nombre de " il y a " rencontree : 
				 *  
				 */

				/*
				 *  On suppose qu'on ne peut pas avoir un retour a la ligne au milieu de "il y a"
				 */

				if (line.contains("il y a ") || line.contains(" il y a ")){
					nbIlya++;
				}


				/*
				 *  Recupere les mots de chaque ligne en ignorant les delimiteurs passes en param
				 */

				st = new StringTokenizer(line,"-?,!. ");
				while (st.hasMoreTokens()){
					String tmp = st.nextToken();


					/*
					 * Seulement les mots de longueurs > 2 chars sont ajoutes au HashMap de l'histogramme
					 */
					/*
					 *  Si on rencontre des mots ayant la meme longueur, on incremente la valeur correspondante (nombre de fois rencontré)
					 */

					if (tmp.length()>2){
						if (histMap.get(tmp.length())!= null){
							histMap.put(tmp.length(), histMap.get(tmp.length()) + 1);
						}else{

							// Sinon, ce mot est insere pour le premiere fois
							histMap.put(tmp.length(), 1);
						}
					}


					/*
					 *  Remplissage de table de hashage contenant tous les mots differents et leurs 
					 */

					/*
					 *  nombres d'occurances dans le texte
					 */

					if (map.get(tmp)==null){ 
						map.put(tmp,1);
					}else{
						map.put(tmp,map.get(tmp) + 1);
					}
				}
			}
			br.close();

		}  catch (IOException e) {
			System.out.println("Erreur : " + e.getMessage());
			e.printStackTrace();
		}
		finally{
			try {
				fr.close();
			} catch (IOException e) {
				System.out.println("Erreur : " + e.getMessage());
				e.printStackTrace();
			}	


			/*
			 *  Les mots de longueurs differentes de 6 lettres
			 */

			for (Map.Entry<String, Integer> entry : map.entrySet()){
				nbMots += entry.getValue();

				if (entry.getKey().length() == 6){
					nbMotsSixLettres++;
				}
				if (entry.getKey().length() == 3){
					nbMotsTroisLettres++;
				}
			}


			/*
			 * l'Entry correspondant au plus long mot
			 */

			for (Map.Entry<Integer, Integer> entry : histMap.entrySet()){

				if (maxEntry==null || entry.getKey() > maxEntry.getKey()){
					maxEntry = entry;
				}
			}

			System.out.println("Nombre de lignes : " + nblignes);
			System.out.println("Nombre de lignes commencant par un tiret : " + nbTirets);
			System.out.println("Nombre de mots : " + nbMots);
			System.out.println("Nombre de \"Maheu\" : " + map.get("maheu"));//+ "  nbMaheu  " + nbMaheu + " nbMaheude " + nbMaheude);
			System.out.println("Nombre de \"il y a\" : " + nbIlya);
			System.out.println("Nombre de mots differents : " + map.size());
			System.out.println("Nombre de mots differents de 6 lettres : " + nbMotsSixLettres);

			System.out.println("Nombre de mots differents de 3 lettres : " + nbMotsTroisLettres);
			System.out.println("Les mots les plus longs : ");


			/*
			 *  Les mots ayant la plus grande longueur
			 */

			for (Entry<String, Integer> entry : map.entrySet()){
				if (entry.getKey().length() == maxEntry.getKey()){
					System.out.println( " >  " + entry.getKey());
				}
			}

		}
	}

}

